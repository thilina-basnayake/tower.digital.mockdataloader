﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using MockServerClientNet.Extensions;
using MockServerClientNet.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MockDataLoader.Models
{
    public class Definition
    {
        public string Url { get; set; }
        public string ContextPath { get; set; }
        public int Port { get; set; }
        public bool Reset { get; set; }
        public ExpectationModel[] Expectations { get; set; }
    }

    public class ExpectationModel
    {
        public string Path { get; set; }
        public ExpectationType Type { get; set; }
        public int Times { get; set; }
        public long TimeToLiveMinutes { get; set; }
        public string RequestMethod { get; set; }
        public object RequestBody { get; set; }
        public HeaderModel[] RequestHeaders { get; set; }
    }

    public enum ExpectationType { FromFile, FromUrl }

    public class FileExpectationModel
    {
        public string Method { get; set; }
        public string Path { get; set; }
        public bool? KeepAlive { get; set; } 
        public bool? UseSSL { get; set; }
        public QueryParameterModel[] QueryStringParameters { get; set; }
        public HeaderModel[] Headers { get; set; }
        public object RequestBody { get; set; }

        public HeaderModel[] ResponseHeaders { get; set; }
        public int ResponseDelayMilliseconds { get; set; }
        public object ResponseBody { get; set; }
        public int ResponseStatusCode { get; set; }
    }

    public class QueryParameterModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class HeaderModel
    {
        public string Name { get; set; }
        public string[] Values { get; set; }
    }

    public static class Extensions
    {
        private static readonly string[] IGNORE_RESPONSE_HEADERS = new[] { "Transfer-Encoding" };
        private static readonly bool FORCE_HTTP = false;

        public static Expectation GetExpectation(this ExpectationModel model)
        {
            var request = new HttpRequest();
            var response = new HttpResponse();

            FileExpectationModel fileModel = null;

            if (model.Type == ExpectationType.FromFile)
            {
                // Open data from file
                string fileBody = System.IO.File.ReadAllText(model.Path);
                fileModel = JsonConvert.DeserializeObject<FileExpectationModel>(fileBody);

            }
            else if (model.Type == ExpectationType.FromUrl)
            {
                // Get Data from Url
                fileModel = model.GetExpectationFromUrl();
            }
            else
            {
                throw new NotSupportedException();
            }

            // Request
            request = request
                .WithMethod(fileModel.Method)
                .WithPath(fileModel.Path)
                .WithSecure(fileModel.UseSSL ?? true)
                .WithKeepAlive(fileModel.KeepAlive ?? false);


            foreach (var header in fileModel.Headers)
                request = request.WithHeader(header.Name, header.Values);

            if (fileModel.QueryStringParameters != null && fileModel.QueryStringParameters.Length > 0)
                request = request.WithQueryStringParameters(fileModel.QueryStringParameters.Select(x => new Parameter(x.Name, x.Value)).ToArray());

            if (fileModel.RequestBody != null)
                request = request.WithBody(JsonConvert.SerializeObject(fileModel.RequestBody));

            // Response
            response = response.WithDelay(TimeSpan.FromMilliseconds(fileModel.ResponseDelayMilliseconds));
            response = response.WithStatusCode(fileModel.ResponseStatusCode);

            if (fileModel.ResponseBody != null)
                response = response.WithBody(JsonConvert.SerializeObject(fileModel.ResponseBody));

            if (fileModel.ResponseHeaders != null && fileModel.ResponseHeaders.Length > 0)
                response = response.WithHeaders(fileModel.ResponseHeaders.Select(x => new Header(x.Name, x.Values)).ToArray());

            var times = model.Times > 0 ? Times.Exactly(model.Times) : Times.Unlimited();
            var ttl = model.TimeToLiveMinutes > 0 ? TimeToLive.Exactly(TimeSpan.FromMinutes(model.TimeToLiveMinutes)) : TimeToLive.Unlimited();
            
            return new Expectation(request, times, ttl).ThenRespond(response);
        }

        public static FileExpectationModel GetExpectationFromUrl(this ExpectationModel model)
        {
            if(model.Type != ExpectationType.FromUrl || string.IsNullOrEmpty(model.Path))
                throw new NotSupportedException();

            var fileModel = new FileExpectationModel();

            using (HttpClient client = new HttpClient())
            {
                var request = new HttpRequestMessage(new HttpMethod(model.RequestMethod), model.Path);

                if (model.RequestHeaders != null)
                    foreach (var header in model.RequestHeaders)
                        request.Headers.TryAddWithoutValidation(header.Name, header.Values);
                
                if(model.RequestBody != null)
                    request.Content = new StringContent(JsonConvert.SerializeObject(model.RequestBody));

                var response = client.SendAsync(request).Result;

                var queryString = HttpUtility.ParseQueryString(request.RequestUri.Query);
                var queryParams = new List<QueryParameterModel>();
                for (int i = 0; i < queryString.Count; i++)
                {
                    var key = queryString.GetKey(i);
                    var value = queryString.Get(i);

                    queryParams.Add(new QueryParameterModel() { Name = key, Value = value });
                }

                fileModel.Method = request.Method.ToString();
                fileModel.Path = request.RequestUri.AbsolutePath;
                fileModel.KeepAlive = true; // todo
                fileModel.UseSSL = !FORCE_HTTP && request.RequestUri.Scheme == "https";
                fileModel.QueryStringParameters = queryParams.ToArray();
                fileModel.Headers = model.RequestHeaders; // request.Headers.Select(x => new HeaderModel() { Name = x.Key, Values = x.Value.ToArray() }).ToArray();
                fileModel.RequestBody = model.RequestBody;
                fileModel.ResponseStatusCode = (int)response.StatusCode;
                fileModel.ResponseDelayMilliseconds = 0;
                fileModel.ResponseHeaders = response.Headers
                    .Where(x => !IGNORE_RESPONSE_HEADERS.Contains(x.Key, StringComparer.InvariantCultureIgnoreCase))
                    .Select(x => new HeaderModel() {Name = x.Key, Values = x.Value.ToArray()}).ToArray();
                fileModel.ResponseBody = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
            }

            return fileModel;
        }
    }
}
