﻿using System;
using MockDataLoader.Models;
using MockServerClientNet;
using MockServerClientNet.Model;
using Newtonsoft.Json;

namespace MockDataLoader
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Reading Definition File");

            string fileBody = System.IO.File.ReadAllText("definition.json");
            Definition definition = JsonConvert.DeserializeObject<Definition>(fileBody);

            Console.WriteLine($"Connecting to Mock Server: {definition.Url}:{definition.Port}");
            Console.WriteLine($"Context Path: {definition.ContextPath ?? ""}");

            var mockServer = new MockServerClient(definition.Url, definition.Port, definition.ContextPath ?? "");
            bool isRunning = mockServer.IsRunning(10, 1000);

            if (definition.Reset)
            {
                Console.WriteLine("Reset Mock Server");
                mockServer.Reset();
            }

            foreach (var expectation in definition.Expectations)
            {
                Console.WriteLine($"Sending Expectation: {expectation.Type}:{expectation.Path}");
                try
                {
                    mockServer.SendExpectation(expectation.GetExpectation());
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception while sending expectation: {ex.GetType()}");
                    Console.WriteLine($"Message: {ex.Message}");
                    Console.WriteLine($"StackTrace: {ex.StackTrace}");
                }
            }

            Console.WriteLine("Completed! Press Enter to Exit");
            Console.ReadLine();
        }
    }
}
